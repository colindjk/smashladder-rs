use std::collections::{HashMap};
use std::io::{self, Write};
use std::ops::{Index,IndexMut};

use cassowary::strength::{ WEAK, MEDIUM, STRONG, REQUIRED };
use cassowary::WeightedRelation::*;

use cassowary::{
    Constraint as CassowaryConstraint, Expression, Solver, Variable,
    AddConstraintError,
};

use termion::event::*;
use termion::cursor;
use termion::input::{TermRead, MouseTerminal};
use termion::raw::IntoRawMode;

pub enum Corner {
    TopRight,
    TopLeft,
    BottomRight,
    BottomLeft,
}

/// Implemented with React and redux in mind. 

// Component which will take up space on the screen. 

/// Enum for a constraining factor of a line.
/// Ideally the bottom right element that takes up a quarter of the screen 
/// would have the constraint `Ratio(1, 4)`, for the X and Y direction, and
/// it's initial location would be @ the center of the element.
/// How is that determined?
pub enum Constraint {
    Percentage(u16),
    Ratio(u16, u16),
    Length(u16), // EQ
    Min(u16), // GE
    Max(u16), // LE
}

pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

pub enum Alignment {

}

type Point = (u32, u32);

/// A line is a vector pointing from point a to b. Pretty straightforward.
type Line = (Point, Point);


/// Make the big rectangle where things can be put.
#[derive(Default, Debug)]
pub struct Rectangle(Line, Line);
// Given a rectangle, we should be able to make constraints about being within
// the boundaries of a rectangle.

/// This way we could implement different shapes, which would have different
/// areas to write in.
/// These areas could be decided inside a struct `new` method, and different
/// types of structs could easily have different shapes...
impl Shape for Rectangle {
    fn get_constraint_vars(&self) -> Vec<Line> {
        vec![]
    }

    // Given a shape, apply constraints to points in that shape.
    fn apply_constraints<T: Shape>(&self, shape: T) -> Vec<Constraint> {
        vec![]
    }
}

/// A shape is a series of lines enclosing a space. 
/// This trait will be the only place constraint logic is written.
pub trait Shape: Default {
    fn get_constraint_vars(&self) -> Vec<Line>;
    fn apply_constraints<T: Shape>(&self, shape: T) -> Vec<Constraint>;
}

pub struct Element {
    x: Variable,
    y: Variable,
    width: Variable,
    height: Variable,
}

impl Element {
    pub fn new() -> Element {
        Element {
            x: Variable::new(),
            y: Variable::new(),
            width: Variable::new(),
            height: Variable::new(),
        }
    }

    pub fn left(&self) -> Variable { self.x }
    pub fn top(&self) -> Variable { self.y }
    pub fn right(&self) -> Expression { self.x + self.width }
    pub fn bottom(&self) -> Expression { self.y + self.height }
}

// Components can be accessed by their names
#[derive(Default, Debug)]
pub struct Component<T: Shape> {
    region: T,
    shape: Vec<Rectangle>, // a series of contiguous rectangles.
    children: HashMap<String, Component<T>>, // Constrain children to region.
}

pub type Updates<'a> = &'a [(Variable, f64)];

// Components can be thought of as hashmaps pointing to other components,
// but also have a region defined by the given rectangular region. The shape
// will determine how the actual component looks, styling can also be
// applied to borders etc. 
impl<T: Shape> Component<T> {
    /// Provided the given parameters, is this structure possible to generate?
    /// If it is, then there's a Some(object), else, None.
    pub fn new(_parent: &Component<T>) -> Option<Self> {
        //parent.get_constraints().map(|c| c);
        None
    }

    // Should recursively apply constraints starting with the root element.
    // Constraints for root element are REQUIRED, since anything else will be
    // out of the window. 
    pub fn draw(&self, ) -> io::Result<()> {
        // What are the actual constraints? Children have to be within the
        // boundaries of the parent Component.

        Ok(())
    }

    /// Apply constraints based on region to children. 
    pub fn apply_constraints(&self, )
        -> Result<Updates, AddConstraintError>
    {

        Ok(&[])
    }
}

impl<T: Shape> Index<&str> for Component<T> {
    type Output = Component<T>;
    fn index(&self, index: &str) -> &Self::Output {
        match self.children.get(index) {
            Some(val) => val,
            None => panic!("Mismatched index for: {:?}", index)
        }

    }
}

// Make it so a component can have things applied to it. 
impl<T: Shape> IndexMut<&str> for Component<T> {
    fn index_mut<'a>(&'a mut self, index: &str) -> &'a mut Component<T> {
        if self.children.contains_key(index) {
            self.children.get_mut(index).unwrap()
        } else {
            self.children.insert(index.to_string(), Component::default());
            self.children.get_mut(index).unwrap()
        }
    }
}

pub fn debug() {
    use std::collections::HashMap;
    fn print_changes(names: &HashMap<Variable, &'static str>,
                     changes: &[(Variable, f64)]) {
        println!("Changes:");
        for &(ref var, ref val) in changes {
            println!("{}: {}", names[var], val);
        }
    }

    let mut root: Component<Rectangle> = Component::default();
    root["child"] = Component::default();

    println!("{:?}", root);
    let mut solver = Solver::new();
    solver.add_constraints(&[]).unwrap();

}

// Given layout rectangles, define constraints n shiz.


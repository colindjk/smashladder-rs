/// Base url for the smashladder API. Uses a macro to express a literal.
/// This should be based on an environment variable so it can be changed for
/// testing purposes.
pub const BASE_URL: &'static str = "https://www.smashladder.com/api/v1";

/// Player
macro_rules! player {
    ($($path:expr);*) => { &( $( concat!("/player", $path) ),* ) }
}

/// Matchmaking
macro_rules! matchmaking {
    ($($path:expr);*) => { &( $( concat!("/matchmaking", $path) ),* ) }
}

/// Match
macro_rules! match_path {
    ($($path:expr);*) => { &( $( concat!("/match", $path) ),* ) }
}

/// Chat
macro_rules! chat {
    ($($path:expr);*) => { &( $( concat!("/chat", $path) ),* ) }
}

/// Site
macro_rules! site {
    ($($path:expr);*) => { &( $( concat!("/site", $path) ),* ) }
}

/// Flairs
macro_rules! flairs {
    ($($path:expr);*) => { &( $( concat!("/flairs", $path) ),* ) }
}

/// Ladders
macro_rules! ladders {
    ($($path:expr);*) => { &( $( concat!("/ladders", $path) ),* ) }
}

/// Rankings
macro_rules! rankings {
    ($($path:expr);*) => { &( $( concat!("/rankings", $path) ),* ) }
}

/// Mod_Match
macro_rules! mod_match {
    ($($path:expr);*) => { &( $( concat!("/mod_match", $path) ),* ) }
}

/// Dolphin
macro_rules! dolphin {
    ($($path:expr);*) => { &( $( concat!("/dolphin", $path) ),* ) }
}

/// Rpg
macro_rules! rpg {
    ($($path:expr);*) => { &( $( concat!("/rpg", $path) ),* ) }
}

/// Account
macro_rules! account {
    ($($path:expr);*) => { &( $( concat!("/account", $path) ),* ) }
}

/// Tournament
macro_rules! tournament {
    ($($path:expr);*) => { &( $( concat!("/tournament", $path) ),* ) }
}

pub const BASIC: &str = player!("/basic");
pub const EDIT_MAINS: &str = player!("/edit_mains");
pub const FRIENDS: &str = player!("/friends");
pub const IGNORED: &str = player!("/ignored");
pub const INIT: &str = player!("/init");
pub const LOGOUT: &str = player!("/logout");
pub const ME: &str = player!("/me");
pub const NOTES: &str = player!("/notes");
pub const PROFILE: &str = player!("/profile");
pub const RANK_HISTORY: &str = player!("/rank_history");
pub const SEARCH: &str = player!("/search");
pub const VIEW_MAINS: &str = player!("/view_mains");

pub const ACCEPT: &str = matchmaking!("/accept");
pub const EDIT_SEARCH_MESSAGE: &str = matchmaking!("/edit_search_message");
pub const END: &str = matchmaking!("/end");
pub const NOTIFICATIONS_FOR_NEW_SEARCHES: &str = matchmaking!("/notifications_for_new_searches");
pub const PLAY_OPTIONS: &str = matchmaking!("/play_options");
pub const REJECT: &str = matchmaking!("/reject");
pub const SET_LADDER_VISIBILITY_IN_MATCHMAKING: &str = matchmaking!("/set_ladder_visibility_in_matchmaking");
pub const SET_LOCATION_RESTRICTIONS: &str = matchmaking!("/set_location_restrictions");
pub const START: &str = matchmaking!("/start");
pub const TOGGLE_DISPLAYED_LIST: &str = matchmaking!("/toggle_displayed_list");
pub const UPDATE_ACTIVE_BUILD_PREFERENCES: &str = matchmaking!("/update_active_build_preferences");
pub const UPDATE_PREFERRED_SEARCH_DISTANCE: &str = matchmaking!("/update_preferred_search_distance");
pub const UPDATE_RANK_PREFERENCE: &str = matchmaking!("/update_rank_preference");
pub const UPDATE_SELECTED_RANK_STATS_SLOT: &str = matchmaking!("/update_selected_rank_stats_slot");
pub const VISIBLE_SEARCHES: &str = matchmaking!("/visible_searches");

pub const CURRENT_MATCH: &str = match_path!("/current_match");
pub const EXAMPLE: &str = match_path!("/example");
pub const EXIT: &str = match_path!("/exit");
pub const GET_CURRENT_MATCHES: &str = match_path!("/get_current_matches");
pub const HISTORY: &str = match_path!("/history");
pub const REPORT: &str = match_path!("/report");
pub const SELECT_CHARACTER: &str = match_path!("/select_character");
pub const SELECT_STAGE: &str = match_path!("/select_stage");

pub const DELETE: &str = chat!("/delete");
pub const DIRECT_MESSAGE_CLOSE: &str = chat!("/direct_message_close");
pub const DIRECT_MESSAGE_LIST: &str = chat!("/direct_message_list");
pub const DIRECT_MESSAGES: &str = chat!("/direct_messages");
pub const FEATURED: &str = chat!("/featured");
pub const FOCUS: &str = chat!("/focus");
pub const JOIN: &str = chat!("/join");
pub const LEAVE: &str = chat!("/leave");
pub const MESSAGES: &str = chat!("/messages");
pub const MY_CHATS: &str = chat!("/my_chats");
pub const PREVIOUS_CHAT_MESSAGES: &str = chat!("/previous_chat_messages");
pub const REMOVE: &str = chat!("/remove");
pub const SEND: &str = chat!("/send");
pub const SEND_TYPING_STATUS: &str = chat!("/send_typing_status");
pub const UNDELETE: &str = chat!("/undelete");
pub const UNREMOVE: &str = chat!("/unremove");
pub const USERLIST: &str = chat!("/userlist");

pub const GLOBAL_NOTIFICATIONS: &str = site!("/global_notifications");

pub const ALL: &str = flairs!("/all");
pub const AVAILABLE_OPTIONS: &str = flairs!("/available_options");
pub const GROUPS: &str = flairs!("/groups");
pub const UPDATE: &str = flairs!("/update");

pub const ACTIVE_PLAYERS: &str = ladders!("/active_players");
pub const ACTIVITY: &str = ladders!("/activity");
pub const CHARACTERS: &str = ladders!("/characters");
pub const GAMES: &str = ladders!("/games");
pub const LADDERS: &str = ladders!("/ladders");
pub const MATCHES: &str = ladders!("/matches");
pub const RECACHE_STATS: &str = ladders!("/recache_stats");
pub const SEASON_CHARACTERS: &str = ladders!("/season_characters");
pub const SEASONS: &str = ladders!("/seasons");

pub const LIST: &str = rankings!("/list");

pub const CANCEL: &str = mod_match!("/cancel");
pub const FEEDBACK_APPROVED: &str = mod_match!("/feedback_approved");
pub const MARK_PLAYER_FAULT: &str = mod_match!("/mark_player_fault");
pub const SET_GAME_WINNER: &str = mod_match!("/set_game_winner");
pub const SET_WINNER: &str = mod_match!("/set_winner");
pub const UNDO_RESULTS: &str = mod_match!("/undo_results");
pub const UNSET_GAME_WINNER: &str = mod_match!("/unset_game_winner");
pub const UPDATE_MATCH: &str = mod_match!("/update_match");
pub const UPDATE_MATCH_FLAG: &str = mod_match!("/update_match_flag");

pub const ALL_BUILDS: &str = dolphin!("/all_builds");
pub const ASK_OTHER_DOLPHIN_SESSIONS_TO_DISABLE: &str = dolphin!("/ask_other_dolphin_sessions_to_disable");
pub const CLOSED_HOST: &str = dolphin!("/closed_host");
pub const CREDENTIALS_LINK: &str = dolphin!("/credentials_link");
pub const INPUT_HOST_CODE: &str = dolphin!("/input_host_code");
pub const OPENED_DOLPHIN: &str = dolphin!("/opened_dolphin");
pub const PLAYER_LIST_UPDATE: &str = dolphin!("/player_list_update");
pub const PREPARE_MATCH_GAME: &str = dolphin!("/prepare_match_game");
pub const READY_TO_JOIN: &str = dolphin!("/ready_to_join");
pub const REPORT_MATCH_GAME: &str = dolphin!("/report_match_game");
pub const SET_HOST_CODE: &str = dolphin!("/set_host_code");
pub const SET_PLAYER_TO_DOLPHIN_SLOT: &str = dolphin!("/set_player_to_dolphin_slot");
pub const SYNC_BUILDS: &str = dolphin!("/sync_builds");
pub const TRIGGER_END_HOST: &str = dolphin!("/trigger_end_host");
pub const TRIGGER_START_GAME: &str = dolphin!("/trigger_start_game");
pub const TRIGGER_START_HOST: &str = dolphin!("/trigger_start_host");
pub const UNSET_DOLPHIN_SLOT: &str = dolphin!("/unset_dolphin_slot");

pub const MATCHMAKE: &str = rpg!("/matchmake");
pub const ATTACK: &str = rpg!("/attack");
pub const DEFEND: &str = rpg!("/defend");
pub const GET_STATS: &str = rpg!("/get_stats");

pub const USERNAME_IS_AVAILABLE: &str = account!("/username_is_available");

pub const INTEREST: &str = tournament!("/interest");
pub const MONTHLY: &str = tournament!("/monthly");
pub const TOGGLE_GAME_PREFERENCE: &str = tournament!("/toggle_game_preference");
pub const TOURNAMENT: &str = tournament!("/tournament");


#![allow(dead_code, unused_imports, unused_variables)]

extern crate cassowary;
extern crate http;
extern crate hyper;
extern crate hyper_tls;
extern crate termion;
extern crate tui;

pub mod api;
pub mod ui;
pub mod app;

use std::io;
use termion::raw::IntoRawMode;
use tui::Terminal;
use tui::backend::TermionBackend;
use tui::widgets::{Widget, Block, Borders};
use tui::layout::{Layout, Constraint, Direction};

fn main() -> Result<(), io::Error> {
    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    terminal.draw(|mut f| {
        // These "chunks" can be split up in many different ways. We must 
        // create a way to organize a layout more easily.
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .margin(0)
            .constraints(
                [
                    Constraint::Percentage(10),
                    Constraint::Percentage(80),
                    Constraint::Percentage(10)
                ].as_ref()
            )
            .split(f.size());

        // This is the systems solution.
        let header = chunks[0];
        let body = chunks[1];
        let footer = chunks[2];

        Block::default()
             .title("Block")
             .borders(Borders::ALL)
             .render(&mut f, header);

        Block::default()
             .title("Block 2")
             .borders(Borders::ALL)
             .render(&mut f, footer);
    })
}

use http;
use hyper;
use hyper::{Request, Response, client, Body};
use hyper::rt::{self, Future, Stream};
use hyper_tls;
use hyper_tls::{HttpsConnector};

use std::io::{self, Write};

use hyper::Client;
use hyper::rt::{self, Future, Stream};

use api;

/// Client used to communicate with the smashladder API.
#[derive(Debug)]
pub struct Client {
    url: &'static str,
    client: client::Client<HttpsConnector<client::HttpConnector>, Body>
}

impl Client {
    /// Creates a client with the default base url.
    pub fn new() -> Result<Self, hyper_tls::Error> {
        let https = HttpsConnector::new(4)?;
        Ok(Client {
            url: api::BASE_URL,
            client: client::Client::builder().build(https)
        })
    }

    pub fn with_url(url: &'static str) -> Result<Self, hyper_tls::Error> {
        let https = HttpsConnector::new(4)?;
        Ok(Client {
            url: url,
            client: client::Client::builder().build(https)
        })
    }

    fn url(&self, path: &str) -> String {
        format!("{}{}", &self.url, path)
    }

    fn basic(user_id: u32) -> hyper::Result<Response<()>> {
        //Request::builder().uri(self.url(api::BASIC))
        unimplemented!()
    }
}

fn fetch_url(url: hyper::Uri) -> impl Future<Item=(), Error=()> {
    let client = Client::new();

    client.get(url)
        // And then, if we get a response back...
        .and_then(|res| {
            println!("Response: {}", res.status());
            println!("Headers: {:#?}", res.headers());

            // The body is a stream, and for_each returns a new Future
            // when the stream is finished, and calls the closure on
            // each chunk of the body...
            res.into_body().for_each(|chunk| {
                io::stdout().write_all(&chunk)
                    .map_err(|e| panic!("example expects stdout is open, error={}", e))
            })
        }).map(|_| {
            println!("\n\nDone.");
        }).map_err(|err| {
            eprintln!("Error {}", err);
        })
}
